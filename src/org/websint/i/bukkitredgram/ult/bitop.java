package org.websint.i.bukkitredgram.ult;

import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Iinputport;

public abstract class bitop {
	public static long rtoi(Iinputport[] c){
		long r=0;
		if(c.length>64){
			throw new RuntimeException("the input too much. it is going to overflow!");
		}
		for(int i=0;i<c.length;i++){
			if(c[i].readBit()){
				long aw=1l<<i;				// Here, 00000001b left move i time will be 00000010b 00000100b ...
															// So, this result is just a byte that the ith places (start from 0) is 1.
				r|=aw;								// So, OR this will set the ith places of r to 1.
			}
		}
		Iinputport[] d=new Iinputport[c.length];
		for (int i = 0; i < d.length; i++) {
			d[i]=c[(c.length-1)-i];
		}
		return r;
	}
	public static void itor(long b,IOutputport[] out){
		for(int i=0;i<out.length;i++){
			if(i>63){
				break;
			}
			long aw=1l<<i;
			long test=b&aw;				// Here, use 00000100b to AND 00011010b, will get all places that is 0 on aw
															// still be 0 and the place that is 1 will be  1 AND 0 = 0. if that place in b is 1
															// than that place will be 1.
			if(test>0){								// If it contains 1?
				out[out.length-1-i].writeBit(true);
			}else{
				out[out.length-1-i].writeBit(false);
			}
		}
	}
	public static byte high4bit(byte b){
		// 01100101b . high = 0110xxxx >> 4
		return (byte)(b>>4);
	}
	public static byte low4bit(byte b){
		// 01100101b . low = (xxxx0101 << 4 = 01010000) >> 4 = 00000101b
		return (byte)((b<<4)>>4);
	}
}
