package org.websint.i.bukkitredgram;

import org.bukkit.Material;
import org.bukkit.block.Block;
/**
 * Output to Block - change block from glass to redstoneblock
 */
public class BlockOutput implements IOutputport {
	protected Block b;
	public Block getBlock(){
		return b;
	}
	public BlockOutput(Block b){
		this.b=b;
	}
	@Override
	public void writeBit(boolean bit) {
		if(bit){
			this.b.setType(Material.REDSTONE_BLOCK);
		}else{
			this.b.setType(Material.GLASS);
		}
	}
	@Override
	public String toString(){
		return "block output at ["+b.getLocation().getBlockX()+","+b.getLocation().getBlockY()+","+b.getLocation().getBlockZ()+"]";
	}
}
