package org.websint.i.bukkitredgram.grams;

import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Igram;
import org.websint.i.bukkitredgram.Iinputport;
/**
 * A demo ( use /gpsd to run :) )
 */
public class demoGram implements Igram {
	
	protected IOutputport out;
	protected boolean nowStatus=false;
	public IOutputport getOut() {
		return out;
	}

	public void setOut(IOutputport out) {
		this.out = out;
	}

	public boolean getNowStatus() {
		return nowStatus;
	}

	public void setNowStatus(boolean nowStatus) {
		this.nowStatus = nowStatus;
	}

	public demoGram(IOutputport out) {
		this.out=out;
	}

	@Override
	public void setup() {
		this.out.writeBit(false);
	}
	
	protected int ticktime=0;
	public int getTicktime() {
		return ticktime;
	}

	public void setTicktime(int ticktime) {
		this.ticktime = ticktime;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	protected int delay=1;

	@Override
	public void loop() {
		if(ticktime==0){
			this.nowStatus=!this.nowStatus;
			this.out.writeBit(nowStatus);
		}
		ticktime++;
		if(ticktime>=delay){
			ticktime=0;
		}
	}

	@Override
	public IOutputport[] getOutputs() {
		return new IOutputport[]{this.out};
	}

	@Override
	public Iinputport[] getInputs() {
		return new Iinputport[]{};
	}

	@Override
	public void pause() {
		this.ticktime=0;
	}

	@Override
	public void resume() {
	}

	@Override
	public void dissetup() {
		this.out.writeBit(false);
	}
	
	@Override
	public String toString(){
		return "timer to "+out.toString()+" with a delay "+delay;
	}
}
