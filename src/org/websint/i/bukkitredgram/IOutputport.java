package org.websint.i.bukkitredgram;

public interface IOutputport {
	public void writeBit(boolean bit);
}
