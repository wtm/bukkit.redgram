package org.websint.i.bukkitredgram;

import org.bukkit.Material;
import org.bukkit.block.Block;
/**
 * read from block - is block powered? (can be indirectly)
 */
public class BlockInput implements Iinputport {

	protected Block b;
	public Block getBlock(){
		return b;
	}
	public BlockInput(Block b){
		this.b=b;
		this.b.setType(Material.IRON_BLOCK);
	}
	@Override
	public boolean readBit() {
		return b.isBlockIndirectlyPowered() || b.isBlockPowered();
	}
	@Override
	public String toString(){
		return "block Input at ["+b.getLocation().getBlockX()+","+b.getLocation().getBlockY()+","+b.getLocation().getBlockZ()+"]";
	}
}
