package org.websint.i.bukkitredgram;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.websint.i.bukkitredgram.grams.demoGram;

public class redgram extends JavaPlugin {
	protected Logger l;
	protected File datafolder;
	protected Configuration minyc;
	protected File scriptfolder;
	protected ArrayList<Igram> runningGrams=new ArrayList<Igram>();
	protected BukkitTask ttk;
	protected URLClassLoader cld;
	@SuppressWarnings({ "deprecation" })
	@Override
    public void onEnable() {
		l=getLogger();
		l.log(Level.WARNING, "This plugin is alpha version. If it has any issues, submit it on github.");
		datafolder=getDataFolder();
		if(!datafolder.isDirectory()){
			datafolder.mkdir();
		}
		scriptfolder=new File(datafolder, "grams");
		if(!scriptfolder.isDirectory()){
			scriptfolder.mkdir();
		}
		final File usrcfg=new File(datafolder, "user.yml");
		try {
			if(!usrcfg.isFile()){
				usrcfg.createNewFile();
			}
			minyc=YamlConfiguration.loadConfiguration(usrcfg);
			new BukkitRunnable() {
				
				@Override
				public void run() {
					try {
						((FileConfiguration)minyc).save(usrcfg);
					} catch (IOException e) {
						e.printStackTrace();
						l.log(Level.WARNING,"! - can not save user settings!");
					}
				}
			}.runTaskTimer(this, 100, 100);
		}catch (IOException e){
			e.printStackTrace();
			l.log(Level.WARNING,"! - will not save any user settings!");
			minyc=new MemoryConfiguration();
		}
		minyc.set("lastrun", new Date().getTime());
		ttk=new runtask(this.runningGrams).runTaskTimer(this, 1, 1);
		try {
			l.log(Level.INFO, "scriptfolder: "+scriptfolder.getAbsolutePath());
			File[] fs=scriptfolder.listFiles();
			l.log(Level.INFO,fs.length+" jars to load.");
			URL[] urls=new URL[fs.length];
			for (int i = 0; i < fs.length; i++) {
				urls[i]=fs[i].toURL();
			}
			cld = new URLClassLoader (urls, this.getClass().getClassLoader());
			// Class classToLoad = Class.forName ("redgrams.Pluser", true, child);
			// World w=Bukkit.getWorld("w");
			// Igram instance = (Igram) classToLoad.getDeclaredConstructor(Iinputport[].class, IOutputport[].class).newInstance();
			// runGram(instance);
		} catch (MalformedURLException | IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
			l.log(Level.WARNING,"! - can not load jars!");
		}
    }
	
	public ConfigurationSection getUserYaml(String playername){
		ConfigurationSection usea=minyc.getConfigurationSection("players");
		if(usea==null){
			usea=minyc.createSection("players");
		}
		ConfigurationSection fusr=usea.getConfigurationSection(playername);
		if(fusr==null){
			fusr=usea.createSection(playername);
		}
		return fusr;
	}
 
    @Override
    public void onDisable() {
    	ttk.cancel();
    	for(int i=0;i<runningGrams.size();i++){
    		stopGram(i);
    	}
    }
    
    public int runGram(Igram ig){
    	ig.setup();
    	runningGrams.add(ig);
    	return runningGrams.size()-1;
    }
    public void stopGram(int i){
    	if(runningGrams.get(i)==null){
    		return;
    	}
    	runningGrams.get(i).dissetup();
    	runningGrams.set(i, null);
    }
    public ConfigurationSection getUserTPortYaml(String playername){
    	ConfigurationSection usr=getUserYaml(playername);
    	ConfigurationSection utp=usr.getConfigurationSection("tmpports");
    	if(utp==null){
    		utp=usr.createSection("tmpports");
    	}
    	return utp;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if(cmd.getName().equalsIgnoreCase("gpsd")){
    		if(args.length>1){
    			return false;
    		}
    		if(!(sender instanceof Entity)){
    			sender.sendMessage("This command can only be called by a Entity");
    			return true;
    		}
    		int delay=1;
    		try {
    			if(args.length==1){
        			delay=Integer.parseInt(args[0]);
        		}
        		demoGram dg=new demoGram(new BlockOutput(((Entity) sender)
    					.getLocation().getBlock()));
        		dg.setDelay(delay);
    			runGram(dg);
    			return true;
    		}catch(NumberFormatException e){
    			return false;
    		}
    	}
    	if(cmd.getName().equalsIgnoreCase("glistrunning")){
    		if(args.length!=0){
    			return false;
    		}else{
    			int val=0;
    			for (int i=0; i<runningGrams.size(); i++){
    				if(runningGrams.get(i)==null){
    					continue;
    				}
    				sender.sendMessage("| "+i+". "+runningGrams.get(i).toString());
    				val++;
    			}
    			sender.sendMessage("There are "+val+" grams running.");
    			return true;
    		}
    	}
    	if(cmd.getName().equalsIgnoreCase("gstop")){
    		if(args.length!=1){
    			return false;
    		}else{
    			try {
    				int idx=Integer.parseInt(args[0]);
    				stopGram(idx);
    				sender.sendMessage("removed!");
    				return true;
    			}catch(NumberFormatException e){
    				return false;
    			}
    		}
    	}
    	if(cmd.getName().equalsIgnoreCase("gtportlist")){
    		if(args.length!=0){
    			return false;
    		}else{
    			ConfigurationSection tps=getUserTPortYaml(sender.getName());
    			Set<String> sskeys=tps.getKeys(false);
    			for (String string : sskeys) {
					sender.sendMessage(" "+string+" | "+tportTostring(tps.getConfigurationSection(string)));
				}
    			sender.sendMessage("there are "+sskeys.size()+" tports added.");
    			return true;
    		}
    	}
    	if(cmd.getName().equalsIgnoreCase("gtportadd")){
    		if(args.length<2 || args.length>3){
    			return false;
    		}
    		String name=args[0];
    		String inout=args[1];
    		String sfl=(args.length==3?args[2]:"");
    		if(name.length()>20){
    			sender.sendMessage("name too long!");
    			return false;
    		}
    		if(!(inout.equalsIgnoreCase("IN") || inout.equalsIgnoreCase("OUT"))){
    			return false;
    		}
    		inout=inout.toUpperCase();
    		Location l;
    		try{
        		if(sfl.matches("^-*\\d+,-*\\d+,-*\\d+$")){
        			int x,y,z;
        			String xyz[]=sfl.split(",");
        			x=Integer.parseInt(xyz[0]);
        			y=Integer.parseInt(xyz[1]);
        			z=Integer.parseInt(xyz[2]);
        			l=new Location(Bukkit.getWorlds().get(0), x, y, z);
        		}else if(sfl.matches("^.+\\.[xyz][+-]=-*\\d+$")){
        			String[] a=sfl.split("\\.");
        			String namerev=a[0];
        			String op=a[1].substring(0, 1); // x y or z
        			String oa=a[1].substring(1, 2); // + or -
        			String ob=a[1].substring(2);    // =\d
        			if(!ob.startsWith("=")){
        				return false;
        			}
        			String oc=a[1].substring(3);
        			int oac=Integer.parseInt(oc); // \d
        			ConfigurationSection upt=getUserTPortYaml(sender.getName());
        			ConfigurationSection tpa=upt.getConfigurationSection(namerev);
        			if(tpa==null){
        				sender.sendMessage("can't find "+namerev);
        				return true;
        			}
        			Location lct=new Location(Bukkit.getWorlds().get(0), tpa.getInt("x"), tpa.getInt("y"), tpa.getInt("z"));
        			if(oa.equals("-")){
        				oac=-oac;
        			}else if(oa.equals("+")){
        				;
        			}else{
        				return false;
        			}
        			if(op.equalsIgnoreCase("x")){
        				lct.add(oac, 0, 0);
        			}else if(op.equalsIgnoreCase("y")){
        				lct.add(0, oac, 0);
        			}else if(op.equalsIgnoreCase("z")){
        				lct.add(0, 0, oac);
        			}else{
        				return false;
        			}
        			l=lct;
        		}else if(sfl.length()==0){
        			if(sender instanceof Entity){
        				l=((Entity)sender).getLocation();
        			}else{
        				sender.sendMessage("You must be a Entity to call this command without position arg.");
        				return true;
        			}
        		}else{
        			return false;
        		}
    		}catch(NumberFormatException e){
    			return false;
    		}
    		ConfigurationSection f=getUserTPortYaml(sender.getName());
    		ConfigurationSection cfgs=f.createSection(name);
    		cfgs.set("type", inout);
    		cfgs.set("x",l.getBlockX());
    		cfgs.set("y",l.getBlockY());
    		cfgs.set("z",l.getBlockZ());
    		return true;
    	}
    	if(cmd.getName().equalsIgnoreCase("grun")){
    		if(args.length<1){
    			return false;
    		}
    		ArrayList<Iinputport> inputs=new ArrayList<Iinputport>();
    		ArrayList<IOutputport> outputs=new ArrayList<IOutputport>();
    		ConfigurationSection cfs=getUserTPortYaml(sender.getName());
    		for(int i=1;i<args.length;i++){
    			String apname=args[i];
    			ConfigurationSection fs=cfs.getConfigurationSection(apname);
    			if(fs==null){
    				sender.sendMessage("tmpport "+apname+" undefined.");
    				return true;
    			}
    			String type=fs.getString("type");
    			if(!fs.getBoolean("~")){
        			if(type.equalsIgnoreCase("in")){
        				inputs.add(new BlockInput(Bukkit.getWorlds().get(0).getBlockAt(fs.getInt("x"), fs.getInt("y"), fs.getInt("z"))));
        			}else if(type.equalsIgnoreCase("out")){
        				outputs.add(new BlockOutput(Bukkit.getWorlds().get(0).getBlockAt(fs.getInt("x"), fs.getInt("y"), fs.getInt("z"))));
        			}else{
        				sender.sendMessage("type invail, configure error. delete tmpport "+apname+" and recreate it.");
        				return true;
        			}
    			}else{
    				int amount=fs.getInt("i");
    				String line=fs.getString("l");
    				int x=fs.getInt("x"), y=fs.getInt("y"), z=fs.getInt("z");
    				for(int j=0;j<amount;j++){
    					if(j!=0){
    						if(line.equalsIgnoreCase("x")){
        						x+=2;
        					}else if(line.equalsIgnoreCase("y")){
        						y+=2;
        					}else if(line.equalsIgnoreCase("z")){
        						z+=2;
        					}
    					}
    					if(type.equalsIgnoreCase("in")){
            				inputs.add(new BlockInput(Bukkit.getWorlds().get(0).getBlockAt(x,y,z)));
            			}else if(type.equalsIgnoreCase("out")){
            				outputs.add(new BlockOutput(Bukkit.getWorlds().get(0).getBlockAt(x,y,z)));
            			}else{
            				sender.sendMessage("type invail, configure error. delete tmpport "+apname+" and recreate it.");
            				return true;
            			}
    				}
    			}
    		}
    		try {
    			Object[] ape=inputs.toArray();
    			Iinputport[] ap=new Iinputport[ape.length];
    			for (int i = 0; i < ape.length; i++) {
					ap[i]=(Iinputport) ape[i];
				}
    			Object[] ope=outputs.toArray();
    			IOutputport[] op=new IOutputport[ope.length];
    			for (int i = 0; i < ope.length; i++) {
					op[i]=(IOutputport) ope[i];
				}
				@SuppressWarnings("rawtypes")
				Class classToLoad = Class.forName (args[0], true, cld);
				@SuppressWarnings("unchecked")
				Igram instance = (Igram) classToLoad.getDeclaredConstructor(Iinputport[].class, IOutputport[].class).newInstance(
						ap, op);
				runGram(instance);
				sender.sendMessage("runned. - "+instance.toString());
			} catch (ClassNotFoundException e) {
				sender.sendMessage(args[0]+" not loaded. put jars in plugins/redgram/grams, reload server and retry.");
			} catch (InstantiationException e) {
				sender.sendMessage(e.toString());
			} catch (IllegalAccessException e) {
				sender.sendMessage(e.toString());
			} catch (IllegalArgumentException e) {
				sender.sendMessage("Oh, the class "+args[0]+" constructor are not complyd to the standards.");
			} catch (InvocationTargetException e) {
				sender.sendMessage(e.getTargetException().toString());
			} catch (NoSuchMethodException e) {
				sender.sendMessage("Oh, the class "+args[0]+" don't have public constructor.");
			} catch (SecurityException e) {
				sender.sendMessage(e.toString());
			}
    		return true;
    	}
    	if(cmd.getName().equalsIgnoreCase("grunsp")){
    		if(args.length!=2){
    			return false;
    		}
    		ConfigurationSection cfs=getUserTPortYaml(sender.getName());
    		ConfigurationSection tps=cfs.getConfigurationSection(args[1]);
    		if(tps==null){
    			sender.sendMessage("tmpport "+args[1]+" dosen't exist.");
    			return true;
    		}
    		try {
    			@SuppressWarnings("rawtypes")
				Class classToLoad = Class.forName (args[0], true, cld);
				@SuppressWarnings("unchecked")
				Igram instance = (Igram) classToLoad.getDeclaredConstructor(Block.class).newInstance(
						Bukkit.getWorlds().get(0).getBlockAt(tps.getInt("x"), tps.getInt("y"), tps.getInt("z")));
				runGram(instance);
				sender.sendMessage("runned. - "+instance.toString());
			} catch (ClassNotFoundException e) {
				sender.sendMessage(args[0]+" not loaded. put jars in plugins/redgram/grams, reload server and retry.");
			} catch (InstantiationException e) {
				sender.sendMessage(e.toString());
			} catch (IllegalAccessException e) {
				sender.sendMessage(e.toString());
			} catch (IllegalArgumentException e) {
				sender.sendMessage("Oh, the class "+args[0]+" constructor are not complyd to the standards.");
			} catch (InvocationTargetException e) {
				sender.sendMessage(e.getTargetException().toString());
			} catch (NoSuchMethodException e) {
				sender.sendMessage("Oh, the class "+args[0]+" don't have public constructor.");
			} catch (SecurityException e) {
				sender.sendMessage(e.toString());
			}
    		return true;
    	}
    	if(cmd.getName().equalsIgnoreCase("gtportrm")){
    		if(args.length!=1){
    			return false;
    		}
    		ConfigurationSection cfg=getUserTPortYaml(sender.getName());
    		cfg.set(args[0], null);
    		return true;
    	}
    	if(cmd.getName().equalsIgnoreCase("gtportrmall")){
    		if(args.length!=0){
    			return false;
    		}
    		ConfigurationSection cfg=getUserTPortYaml(sender.getName());
    		Set<String> ss=cfg.getKeys(false);
    		for (String string : ss) {
				cfg.set(string,null);
			}
    		sender.sendMessage(ss.size()+" tmpports removed.");
    		return true;
    	}
    	if(cmd.getName().equalsIgnoreCase("gtpt")){
    		if(args.length!=4){
    			return false;
    		}
    		if(!(sender instanceof Entity)){
    			sender.sendMessage("this command can only call by a Entity.");
    			return false;
    		}
    		try{
    			String prefix=args[0];
        		int amount=Integer.parseInt(args[1]);
        		String line=args[2];
        		String type=args[3];
        		if(!(type.equalsIgnoreCase("in") || type.equalsIgnoreCase("out"))){
        			return false;
        		}
        		if(!(line.equalsIgnoreCase("x")||line.equalsIgnoreCase("y")||line.equalsIgnoreCase("z"))){
        			return false;
        		}
        		Location startloc=((Entity)sender).getLocation();
        		ConfigurationSection cfs=getUserTPortYaml(sender.getName());
        		ConfigurationSection f=cfs.createSection(prefix);
        		f.set("type", type.toUpperCase());
        		f.set("i", amount);
				f.set("l", line);
				f.set("~",true);
        		for(int i=0;i<amount;i++){
        			ConfigurationSection tcfs=cfs.createSection(prefix+i);
        			tcfs.set("type", type.toUpperCase());
        			if(i==0){
        				tcfs.set("i", amount);
        				tcfs.set("l", line);
        				tcfs.set("~",true);
        			}
        			/*
        			 Returns:
							***** the same location
        			 */
        			if(line.equalsIgnoreCase("x")){
        				startloc.add(2, 0, 0).getBlock().setType(Material.WOOL);
        			}else if(line.equalsIgnoreCase("y")){
        				startloc.add(0, 2, 0).getBlock().setType(Material.WOOL);
        			}else if(line.equalsIgnoreCase("z")){
        				startloc.add(0, 0, 2).getBlock().setType(Material.WOOL);
        			}
        			tcfs.set("x", startloc.getBlockX());
        			tcfs.set("y", startloc.getBlockY());
        			tcfs.set("z", startloc.getBlockZ());
        			if(i==0){
        				f.set("x", startloc.getBlockX());
            			f.set("y", startloc.getBlockY());
            			f.set("z", startloc.getBlockZ());
        			}
        		}
        		sender.sendMessage(amount+" tmpport gened. use "+prefix+" to use all.");
        		return true;
    		}catch(NumberFormatException e){
    			return false;
    		}
    	}
    	return false; 
    }
    public String tportTostring(ConfigurationSection tport){
    	return tport.getString("type")+"[ "+tport.getInt("x")+", "+tport.getInt("y")+", "+tport.getLong("z")+" ]";
    }
}
