package org.websint.i.bukkitredgram;

import java.util.ArrayList;

import org.bukkit.scheduler.BukkitRunnable;

public class runtask extends BukkitRunnable {
	protected ArrayList<Igram> r;
	public runtask(ArrayList<Igram> r) {
		this.r=r;
	}
	@Override
	public void run() {
		for (Igram g : r) {
			if(g!=null){
				// redgram.invokeIgram(g, "loop");
				g.loop();
			}
		}
	}
}
