package org.websint.i.bukkitredgram;
/**
 * write a gram now! very easy.
 */
public interface Igram {
	/**
	 * call one time, do some init.
	 */
	public void setup();
	/**
	 * call one time per tick.
	 */
	public void loop();
	/**
	 * @return a array of all outputs.
	 */
	public IOutputport[] getOutputs();
	/**
	 * @return a array of all inputs.
	 */
	public Iinputport[] getInputs();
	/**
	 * call when player try to pause the gram.
	 * you shoulden't do anything editing outputs when you has been paused.
	 */
	public void pause();
	/**
	 * call when player try to continue the running of the gram.
	 */
	public void resume();
	/**
	 * call when /gstop or server stop.
	 */
	public void dissetup();
	/**
	 * give me a friendly text for /glistrunning.
	 * @return
	 */
	public String toString();
}
